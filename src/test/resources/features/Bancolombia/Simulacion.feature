@Regresion
Feature: Verificar el funcionamiento de la pantalla de simulacion de creditos 
expuesta por el grupo Bancolombia, en cuanto a la prsentacion de los valores calculados.

  @CasoExitoso
  Scenario: Verificar el funcionamiento del simulador de calcular y 
  presentar el resultado en el campo Cueta mensual
    Given Ingreso a la pantalla simular
    And Ingreso los campos en el formulario
     | Tipo_Simulacion | Fecha     | Tipo_Tasa | Producto_Cred              | Seguro_Desempleo | Plazo | Valor    |
     |Simula tu Cuota  |1990-10-13 | Tasa Fija | Crédito de Libre Inversión | 1                |  36   | 10000000 |
     
    When Doy click en el boton validar   
    Then Verifico que se presente el resultado
    
     @CasoAlterno
  Scenario: Verificar que se presente mensaje del validacion al simular
    Given Ingreso a la pantalla simular
    And Ingreso los campos en el formulario
     | Tipo_Simulacion | Fecha     | Tipo_Tasa | Producto_Cred              | Seguro_Desempleo | Plazo | Valor    |
     |Simula tu Cuota  |           | Tasa Fija | Crédito de Libre Inversión | 1                |  36   | 10000000 |
     
    When Doy click en el boton validar   
    Then Verifico que se presente validacion
   

