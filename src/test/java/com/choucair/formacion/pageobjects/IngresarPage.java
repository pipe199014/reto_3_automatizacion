package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas/productos-servicios/creditos/consumo/libre-inversion/simulador-credito-consumo")
public class IngresarPage extends PageObject {
	
	// Label Simulacion
			@FindBy(xpath = "//*[@id=\"for-detail\"]/div[1]/h1")
			public WebElementFacade lblSimular;
			

			public void VerificaHome() {
				String labelv = "Simula tu Crédito";
				String strMensaje = lblSimular.getText();
				assertThat(strMensaje, containsString(labelv));
			}

}
