package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.math.BigDecimal;
import java.util.List;
import java.util.Properties;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebElement;

import com.ibm.icu.text.DecimalFormat;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SimulacionPage extends PageObject {
	static Properties propiedades;
	// Campo tipo Simulacion
	@FindBy(name = "comboTipoSimulacion")
	public WebElementFacade txtTipoSimulacion;
	// Campo fecha
	@FindBy(name = "dateFechaNacimiento")
	public WebElementFacade txtFecha;
	// Campo tipo_tasa
	@FindBy(name = "comboTipoTasa")
	public WebElementFacade txtTipo_Tasa;
	// Campo ProductoCredr
	@FindBy(name = "comboTipoProducto")
	public WebElementFacade txtTipo_Producto;
	// Campo SeguroDesempleo
	@FindBy(name = "checkSeguroDesempleo")
	public WebElementFacade chkSeguro;
	// Campo plazo
	@FindBy(name = "textPlazoInversion")
	public WebElementFacade txtPlazo;
	// Campo valor
	@FindBy(name = "textValorPrestamo")
	public WebElementFacade txtValor;

	// Boton validar
	@FindBy(xpath = "//*[@id=\"sim-detail\"]/form/div[8]/button")
	public WebElementFacade btnSimular;

	// Propiedades
	public void Tipo_Simulacion(String datoPrueba) {
		txtTipoSimulacion.click();
		txtTipoSimulacion.sendKeys(datoPrueba);
		tiempo(1000);
	}

	public void Fecha_Nacimiento(String datoPrueba) {
		txtFecha.click();
		txtFecha.clear();
		txtFecha.sendKeys(datoPrueba);
		tiempo(1000);
	}

	public void Tipo_Tasa(String datoPrueba) {
		txtTipo_Tasa.click();
		txtTipo_Tasa.sendKeys(datoPrueba);
		tiempo(1000);

	}

	public void Tipo_Productos(String datoPrueba) {
		txtTipo_Producto.click();
		txtTipo_Producto.sendKeys(datoPrueba);
		tiempo(1000);

	}

	public void Seguro_Desempleo(String datoPrueba) {
		chkSeguro.click();
		tiempo(1000);

	}

	public void Plazo(String datoPrueba) {
		txtPlazo.click();
		txtPlazo.clear();
		txtPlazo.sendKeys(datoPrueba);
		tiempo(1000);

	}

	public void Valor_Prestamo(String datoPrueba) {
		txtValor.click();
		txtValor.clear();
		txtValor.sendKeys(datoPrueba);
		tiempo(1000);

	}

	public void Simular() {
		btnSimular.click();
		tiempo(1000);
	}

	public void tiempo(int tiempo) {

		try {
			Thread.sleep(tiempo);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/////////////////////////////////// Campos de
	/////////////////////////////////// validación///////////////////////////////////////

	@FindBy(xpath = "//*[@id=\"sim-results\"]/div[1]/table/tbody/tr")
	public List<WebElement> tblFila;

	public void form_sin_errores() {
		String CoutaMensual = "";
		String P = txtValor.getTextValue();
		String i = "";
		String n = txtPlazo.getTextValue();

		int cont = 0;

		for (WebElement trElement : tblFila) {
			List<WebElement> tblColumna = trElement.findElements(By.xpath("td"));

			int cont2 = 0;
			for (WebElement tdElement : tblColumna) {
				if (cont == 1 && cont2 == 1) {
					i = tdElement.getText();
				}

				if (cont == 2 && cont2 == 2) {
					CoutaMensual = tdElement.getText();
					System.out.println(tdElement.getText());

				}
				cont2++;
			}
			cont++;
		}

		// VerificoFormula
		
		

		String repla = CoutaMensual.replace("$", "").replace(",", "");
		float CuotaMensualNum1 = Float.parseFloat(repla);
		BigDecimal CuotaMensualNum = new BigDecimal(CuotaMensualNum1);

		String Valor = P.replace("$", "").replace(",", "");
		float ValorNum2 = Float.parseFloat(Valor);
		BigDecimal ValorNum = new BigDecimal(ValorNum2);

		String Interes = i.replace("%", "");
		//float inte = Float.parseFloat(Interes);
		BigDecimal InteresNum = new BigDecimal(Interes).divide(new BigDecimal(100));
		//float InteresNum = inte / 100;

		int NumeroPeriodo = Integer.parseInt(n);
		
		Double exp = Math.pow((1 + InteresNum.doubleValue()), NumeroPeriodo); 		
		Double FormulaFinal = ValorNum.doubleValue() *(InteresNum.doubleValue()*(exp)/((exp)-1)); 
	
		BigDecimal Formulafinalok =  new BigDecimal (FormulaFinal);

		///////

		String Num1 = String.format("%.0f", Formulafinalok);
		String Num2 = String.format("%.0f", CuotaMensualNum);

		assertThat(Num1, containsString(Num2));
		// MatcherAssert.assertThat("Valor Exacto", Num1 == CuotaMensualNum);

	}

	public void form_con_errores() {

	}
}
