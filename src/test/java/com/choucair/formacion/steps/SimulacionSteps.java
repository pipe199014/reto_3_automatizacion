package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.SimulacionPage;

import net.thucydides.core.annotations.Step;

public class SimulacionSteps 
{
	SimulacionPage simulacionPage;
	
	@Step
	public void diligenciar_form_simulacion(List<List<String>> data, int id) {
		simulacionPage.Tipo_Simulacion(data.get(id).get(0).trim());
		simulacionPage.Fecha_Nacimiento(data.get(id).get(1).trim());
		simulacionPage.Tipo_Tasa(data.get(id).get(2).trim());
		simulacionPage.Tipo_Productos(data.get(id).get(3).trim());
		simulacionPage.Seguro_Desempleo(data.get(id).get(4).trim());
		simulacionPage.Plazo(data.get(id).get(5).trim());
		simulacionPage.Valor_Prestamo(data.get(id).get(6).trim());
		

	}
	
	@Step
	public void Simular() throws InterruptedException
	{
		simulacionPage.Simular();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		simulacionPage.form_sin_errores();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_con_errores() {
		simulacionPage.form_con_errores();
	}

}
