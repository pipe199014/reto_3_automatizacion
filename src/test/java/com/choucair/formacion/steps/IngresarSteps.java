package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.IngresarPage;

import net.thucydides.core.annotations.Step;

public class IngresarSteps 
{
	IngresarPage ingresarPage;
	
	@Step
	public void IngresarPantallaSimulacion()
		{
			//a. Abrir navegador con la url de prueba
		ingresarPage.open();
		ingresarPage.VerificaHome();
		}
	
}
